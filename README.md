# anti-walkman
## Concept
The idea of the "anti-Walkman" arises as a counter-position to a device that drastically changed the relationship between humans and technology. In Andres Colmenares' words “With more than 400 million units sold, the almost 40-year-old Walkman changed our relationship with technology forever”. The idea of being in your own personal bubble in a public space evolved into what we all know as the smartphone. Humans have become so "over-connected" to the virtual world that we are disconnected from our surroundings and the present moment. The anti-Walkman is a speculative artifact to connects us to the right-here / right-now and minimize the noise of today's hyper-connectedness. This artifact is the first in a series of “mindful machines”, minimal and specific devices that have unique functions to enable you to be in the present moment.

## Purpose
The “anti-Walkman” bracelet is an acrylic lightweight bracelet with a compass that indicates the direction you want to go. Without the excess of information that google maps would give you, the ‘anti-Walkman’ is precise and will not generate extra distractions, it will simply indicate where you have to turn so that you can connect with your surroundings, whether you are walking, hiking or riding a bike.
![purpose](images/11miro.png)

## Project execution
### First Step: 
We started by making some diagrams of the idea by hand. The main idea that came to our minds was starting from a wearable, a fanny pack, a headband or a bracelet. We defined that the best option would be a bracelet (since it could be an object that could be adapted to other kinds of mobility systems, such as a bicycle).
<br>
![sketches](images/01sketches.jpeg)
<br>
### Second step: 
We investigated what material we could use. The options were MDF, acrylic, silicone or a natural textile. After analyzing which material had the best characteristics, we defined that we would use acrylic because it can be laser cut, and would be able to withstand outdoor conditions.
<br>
![materials](images/02materials.jpg)
<br>
### Third step: 
We started drawing the geometries in rhino and testing on the different possibilities for making a super flexible living hinge pattern (#1 - #6 options).
<br>
![rhino](images/03rhino.png)
<br>
![models](images/05prototipos.jpg)
<br>
![rhino](images/04rhino.png)
<br>
![models](images/06prototipos.jpg)
<br>
![rhino](images/07prototipos.jpg)

### Fourth step: 
After several iterations of trial and error, we determined that the latest geometry was the most flexible, and could bend around one's wrist.

## Design process & files
Please see the cad_files folder for .3dm files, ready to be laser cut. Below is the system diagram with the different elements of the artifact:
<br>
![prototype](images/12designelements.jpg)

## Fabrication process
The goal of this project was to practice laser cutting with different materials and geometries. Throughout our design iterations, we gained insight into the properties of acrylic and MDF.

### Focusing the laser cutter
The most important step for laser cutting is to properly focus the laser, i.e. fine-tuning the distance between the laser and the material. Indeed, the laser power will be concentrated at the exact focal length from the lens of the laser. If the object is any closer or farther, the laser power will be to diffuse to cut. The sheet of material to be cut needs to be perfectly flat, so that the focusing will work uniformly across its span. Once the laser is focused, and before cutting, it is imperative to turn on the ventilator,. so as to evacuate any toxic fumes.

### Setting material parameters
The three basic laser parameters that can be adjusted for each material is the power (W), the frequency (Hz) and the speed (mm/s). These are determined by the nature of the material, its thickness and the type of geometry to be cut. To cut 3mm thick transparent acrylic, use 60 W, 0.5 mm/s and 20 000 Hz. For MDF, use 55 W, 0.5 mm/s and 1 000 Hz.

### Cut file
Once the parameters have been set, the file is ready to be cut. It is helpful to cut a small circle as a test, to ensure the parameters work properly. After a week of intense use, the laser lens was dirty, so it wouldn't cut through. We had to disassemble the laser and clean the lens.

## Final prototype
Here is our final prototype. It can be worn on the wrist, or attached to a bicycle's handlebars. The golden arrow indicates the direction to follow.
<br>
![final](images/08pic.jpg)
<br>
![final](images/09pic.jpg)
<br>
![final](images/10pic.jpg)

## Future developments
The major next step will be integrating the electronics to the navigation bracelet. We will require an inertial measurement unit (IMU) to determine the orientation of the device, a GPS chip to determine its location, a microcontroller to compute the direction of the destination and a small display to show you the way (or a stepper motor connected to an arrow). <br>
An alternative embodiment of the anti-walkman concept  would involve 5 buttons to record emotions (e.g. happiness, sadness, fear, anger and surprise) at a given place and time, in order to crowdsource a psychogeographical map of the city. The display would be replaced by an array of colored buttons.

## The dream team
Designed with love in Barcelona (ES) and León (MX) by:<br>
<a href="https://josefina_nano.gitlab.io/mdef-website/">Josefina Nano</a><br>
<a href="https://paco_flores.gitlab.io/mdef-2021/">Paco Flores</a><br>
<a href="https://clement_rames.gitlab.io/mdef-website/">Clément Rames</a><br>